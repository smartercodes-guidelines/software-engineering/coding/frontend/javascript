# Javascript

This project Frontend>Javascript also exists in [Backend>Javascript](https://gitlab.com/smarter-codes/guidelines/software-engineering/coding/backend/javascript)

Should we maintain any issues and README.md file here? If not then please make your contributions only in Backend>Javascript and leaves this message as it is

I do think however that there would be several guidelines pertaining to Frontend>Javascript that do not apply to Backend>Javascript. If that is the case, please contrbute here
